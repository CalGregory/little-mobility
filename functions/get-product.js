const { postToShopify } = require("./utils/postToShopify");

exports.handler = async (event) => {
  const { productHandle } = JSON.parse(event.body)
  try {
    console.log("--------------------------------");
    console.log("Retrieving product list...");
    console.log("--------------------------------");
    const shopifyResponse = await postToShopify({
      query: `query productByHandle($handle: String!) {
        productByHandle(handle: $handle) {
          title
          description
          descriptionHtml
          vendor
          tags
          seo {
            description
            title
          }
          variants(first: 50) {
            edges {
              node {
                id
                title
                quantityAvailable
                priceV2 {
                  amount
                }
              }
            }
          }
          rating: metafield(namespace: "reviews", key: "rating") {
            value
          }
          rating_count: metafield(namespace: "reviews", key: "rating_count") {
            value
          }
          specifications: metafield(namespace: "my_fields", key: "specifications") {
            value
          }
          main_collection: metafield(namespace: "my_fields", key: "main_collection") {
            value
          }
          images(first: 4) {
            edges {
              node {
                url
                altText
              }
            }
          }
        }
      }`,
      variables: {
        handle: productHandle,
      },
    });

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type',
      },
      body: JSON.stringify(shopifyResponse),
    };
  } catch (error) {
    console.log(error);
  }
};
