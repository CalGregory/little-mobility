const { postToShopify } = require("./utils/postToShopify");

exports.handler = async (event) => {
  const { collectionHandle } = JSON.parse(event.body)
  try {
    console.log("--------------------------------");
    console.log("Retrieving product list...");
    console.log("--------------------------------");
    const shopifyResponse = await postToShopify({
      query: `query collectionByHandle($handle: String!) {
        collection(handle: $handle) {
          title
          description
          image {
            altText
            url
          }
          metafield(namespace: "my_fields", key: "header_image") {
            value
          }
          products(first: 40) {
            edges {
              node {
                title
                id
                title
                handle
                tags
                rating: metafield(namespace: "reviews", key: "rating") {
                  value
                }
                rating_count: metafield(namespace: "reviews", key: "rating_count") {
                  value
                }
                specifications: metafield(
                  namespace: "custom"
                  key: "filters"
                ) {
                  value
                }
                featuredImage {
                  url
                  altText
                }
                variants(first: 1) {
                  edges {
                    node {
                      id
                      price {
                        amount
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }`,
      variables: {
        handle: collectionHandle,
      },
    });

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type',
      },
      body: JSON.stringify(shopifyResponse),
    };
  } catch (error) {
    console.log(error);
  }
};
