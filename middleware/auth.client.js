export default function({ redirect }) {
    const user = localStorage.getItem('userAccessToken')
    if (!user) {
      return redirect('/account/login')
    }
  }