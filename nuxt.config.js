const axios = require('axios')
export default {
  target: "static",
  // ssr: true,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Mobility Scooters & Wheelchairs from Solent Mobility Centre",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      {
        hid: 'description',
        name: 'description',
        content: 'Solent Mobility Centre offer a selection of Mobility Scooters, Powerchairs, Wheelchairs and Stairlifts in Hampshire. We are based in Gosport, Lee-on-the-solent!'
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "preconnect", href: "https://fonts.googleapis.com" },
      {
        rel: "preconnect",
        href: "https://fonts.gstatic.com",
        crossorigin: true,
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&family=Varela+Round&display=swap",
        media: "print",
        onload: "this.media='all'",
      },
    ],
    script: [
      {src: '/js/hotjar.js'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/fontawesome",
    '@/modules/routes'
  ],

  plugins: [
    "~/plugins/helpers",
    { src: '~/plugins/vue-menu.js', ssr: false }
],

  fontawesome: {
    icons: {
      solid: [
        "faMagnifyingGlass",
        "faUser",
        "faShoppingBasket",
        "faEnvelope",
        "faMapLocation",
        "faPhone",
        "faComments",
        "faChevronDown",
        "faChevronRight",
        "faWheelchair",
        "faSquarePlus",
        "faMap",
        "faCarBattery",
        "faCheck",
        "faSquareCheck",
        "faStar",
        "faBars",
        "faX",
      ],
      brands: ["faFacebookF", "faInstagram", "faGoogle", "faTwitter"],
    },
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    "@nuxt/image",
    [
      "nuxt-compress",
      {
        gzip: {
          threshold: 8192,
        },
        brotli: {
          threshold: 8192,
        },
      },
    ],
    "@nuxtjs/sitemap",
    "@nuxtjs/apollo",
    "@nuxtjs/tailwindcss",
    '@nuxt/http',
    'vue-plausible'
  ],

  plausible: {
    domain: "solentmobility.co.uk",
    trackLocalhost: true
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: "/",
  },

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint:
          "https://solent-mobility.myshopify.com/api/2023-04/graphql.json",
        httpLinkOptions: {
          headers: {
            "Content-Type": "application/json",
            "X-Shopify-Storefront-Access-Token":
              "616d24494811c57dddb1825a8231472e",
          },
        },
        persisting: false,
      },
      // admin: {
      //   httpEndpoint:
      //     "https://solent-mobility.myshopify.com/admin/api/2022-01/graphql.json",
      //   httpLinkOptions: {
      //     headers: {
      //       "Content-Type": "application/graphql",
      //       "X-Shopify-Access-Token": "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",
      //     },
      //   },
      //   persisting: false,
      // },
    },
  },

  image: {
    domains: ["https://solent-mobility.imgix.net", "cdn.shopify.com"],
    imgix: {
      baseURL: "https://solent-shopify.imgix.net",
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "en",
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // fix to work with swiperjs 8 - need to run with standalone:true. That can make some troubles.
    standalone: true,
    extend(config, ctx) {
      // fix to work with swiperjs 8 add needed deps. you can get them from error when doing nuxt generate
      config.externals = [
        {
          encoding: 'encoding',
        },
      ]
    },
  },

  generate: {
    crawler: true,
    subFolders: false,
    fallback: true,
    concurrency: 120, //only build 250 at a time based on the api rate limit
    interval: 100, //delay by 0.1s
  },

  router: {
    trailingSlash: false,
    middleware: "redirect",
  },
  sitemap: {
    hostname: "https://solentmobility.co.uk",
    gzip: true,
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date()
    }
  },
  devServerHandlers: [],
  env: {
    SHOPIFY_API_ENDPOINT: "https://solent-mobility.myshopify.com/api/2023-04/graphql.json",
    SHOPIFY_STOREFRONT_API_TOKEN: "616d24494811c57dddb1825a8231472e"
  },
};
