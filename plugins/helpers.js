import Vue from "vue";
Vue.mixin({
  methods: {
    currencyCovert(price) {
      let currency = Intl.NumberFormat("en-GB");
      return currency.format(price);
    },
    capitalizeText(str) {
      var splitStr = str.toLowerCase().split(' ');
      for (var i = 0; i < splitStr.length; i++) {
          // You do not need to check if i is larger than splitStr length, as your for does that for you
          // Assign it back to the array
          splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
      }
      // Directly return the joined string
      return splitStr.join(' '); 
    },
    imageFix(url) {
      let i = url.split("/");
      return i.pop();
    },
    hireFix(url) {
      console.log(url)
      let i = url.replace("-", " ")
      console.log(i)
      // if (i.includes('scooter')) {
      //   let e = i.replace("s-hire", "")
      //   return e
      // } else {
      //   let e = i.replace("s-hire", "")
      //   return e
      // }
      return i.replace("s-hire", "")
    }
    
  },
});
