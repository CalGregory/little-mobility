// Graph Queries
import {
  mainCategories,
  subCategories,
  customer,
} from "~/gql/queries/Shopify.gql";

export const state = () => ({
  loading: 0,
  checkout: {
    id: "",
    webUrl: "",
  },
  categoryList: [],
  subCategory: [],
  customerData: {},
});

export const mutations = {
  START_LOADING(state) {
    state.loading = 1;
  },
  FINISH_LOADING(state) {
    // Don't let this go below zero, so it can't get stuck in loading
    if (state.loading > 0) {
      state.loading -= 1;
    }
  },
  SET_COLLECTIONS(state, categoryList) {
    state.categoryList = categoryList;
  },
  SET_SUB(state, payload) {
    state.subCategory = payload;
  },
  SET_CUSTOMER(state, payload) {
    state.customerData = payload;
  },
};

export const actions = {
  async GET_CUSTOMER({ commit }) {
    commit("START_LOADING");
    let client = this.app.apolloProvider.defaultClient;
    let key = JSON.parse(localStorage.getItem("userAccessToken"));
    const customerData = await client.query({
      query: customer,
      variables: {
        customerAccessToken: key.accessToken,
      },
      update: (data) => data,
    });
    commit("SET_CUSTOMER", customerData.data.customer);
    commit("FINISH_LOADING");
  },
  // Retrieve Main Collections
  async GET_COLLECTIONS({ commit }) {
    commit("START_LOADING");
    let client = this.app.apolloProvider.defaultClient;
    const collections = await client.query({
      query: mainCategories,
      update: (data) => data,
    });
    commit("SET_COLLECTIONS", collections);
    commit("FINISH_LOADING");
  },
  async GET_SUBCOLLECTIONS({ commit }, category) {
    const subCats = [
      {
        category: "mobility-scooters",
        query:
          `title:'Pavement Mobility Scooters' OR title:'Travel Mobility Scooters' OR title:'6 to 8 Mph Mobility Scooters' OR title:'Car Boot Scooters' OR title:'Aluminium Mobility Scooters' OR title:'Cabin & All Terrain Mobility Scooters' OR title:'Refurbished Mobility Scooters' 
          OR title:'Mobility Scooter Extras' OR title:'Mobility Scooter Accessories' OR title:'Mobility Scooter Covers' OR title:'Mobility Scooter Cushions' OR title:'Mobility Scooter Ramps' OR title:'Mobility Scooter Standard Ramps' OR title:'Mobility Scooter Folding Ramps' 
          OR title:'Mobility Scooter All Ramps' OR title:'Mobility Scooter Chargers' OR title:'Mobility Scooter Batteries'`,
      },
      {
        category: "powerchairs",
        query:
          `title:'Folding Powerchairs' OR title:'Travel Powerchairs' OR title:'Prescription Powerchairs' OR title:'Powerchair Extras'
          OR title:'Powerchair Extras' OR title:'Powerchair Accessories' OR title:'Powerchair Covers' OR title:'Powerchair Cushions' OR title:'Powerchair Ramps' OR title:'Powerchair Standard Ramps' OR title:'Powerchair Folding Ramps' 
          OR title:'Powerchair All Ramps' OR title:'Powerchair Chargers' OR title:'Powerchair Batteries'`,
      },
      {
        category: "wheelchairs",
        query:
          "'Transit Wheelchairs' OR title:'Self Propelled Wheelchairs' OR title:'Bariatric Wheelchairs' OR title:'Refurbished Wheelchairs' OR title:'Lightweight Wheelchairs' OR title:'Prescription Wheelchairs' OR title:'Wheelchair Extras' OR title:'Wheelchair Accessories' OR title:'Wheelchair Cushions' OR title:'Wheelchair Wet Weather' OR title:'Wheelchair Ramps' OR title:'Wheelchair Standard Ramps' OR title:'Wheelchair Folding Ramps' OR title:'Wheelchair All Ramps' OR title:'Wheelchair Hoists' OR title:'Wheelchair Portable Hoists' OR title:'Wheelchair Ceiling Hoists' ",
      },
      {
        category: "daily-living-aids",
        query: `title:'Tri Walkers' OR title:'Walking Sticks' OR title:'Crutches' OR title:'3 Wheel Walkers' OR title:'4 Wheel Walkers' OR title:'Trolleys' OR title:'Walking Frames' OR title:'Wheeled Frames' OR title:'Ferrules' OR title:'Household Aids' OR title:'Furniture Raisers' OR title:'Grab Rails' OR title:"Reacher's and Grabbers" OR title:'Magnifying' OR title:'Safety and Security' OR title:'Steps and Stools' OR title:'Tables and Trays' OR title:'Transfer Aids' OR title:'Health and Wellbeing' OR title:'Pill Boxes' OR title:'Pill Cutters and Dispensers' OR title:'Foot Circulation' OR title:'Exercise and Rehab' OR title:'Washing and Hygiene' OR title:'Personal Comfort' OR title:'Back Supports' OR title:'Cushions' OR title:'Dressing Accessories' OR title:'Footcare'`,
      },
      {
        category: "furniture",
        query: "title:'Rise and Recliners' OR title:'Single Motor Rise and Recliners' OR title:'Dual Motor Rise and Recliners' OR title:'Fireside Chairs' OR title:'2 & 3 Seater Sofas' OR title:'Porters Chairs' OR title:'Static Chairs' OR title:'Beds' OR title:'Electric Adjustable Beds' OR title:'Profiling Beds' OR title:'Care Beds' OR title:'Refurbished Beds' OR title:'Mattresses' OR title:'Care Mattresses' OR title:'Standard Mattresses' OR title:'Furniture Accessories' OR title:'Overbed Table' OR title:'Cushions' OR title:'Bedroom Accessories' OR title:'Bed Rails' ",
      },
      {
        category: "hoists",
        query: "title:'Portable Hoists' OR title:'Overhead Hoists' OR title:'Stand Aids' OR title:'Hoist Extras' OR title:'Slings' OR title:'Moving and Handling Aids' OR title:'Hoist Batteries'",
      },
      {
        category: "incontinence",
        query: "title:'Pads' OR title:'Stretch Pads' OR title:'TENA' OR title:'Bed Pads' OR title:'Disposable Bed Pads' OR title:'Washable Bed Pads' OR title:'Toileting Aids' OR title:'Commodes' OR title:'Commode Pots & Liners' OR title:'Hygine and Protection' OR title:'Disposable Gloves' OR title:'Washers and Wipes' OR title:'Face Masks' ",
      },
    ];
    let client = this.app.apolloProvider.defaultClient;
    let filteredSub = subCats.filter((x) => x.category == category);
    let { data, error } = await client.query({
      query: subCategories,
      variables: {
        query: filteredSub[0].query,
      },
      update: (data) => data,
    });
    console.table(data)
    commit("SET_SUB", data.collections.edges);
  },
};
