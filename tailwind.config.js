module.exports = {
  darkMode: false, // or 'media' or 'class'
  prefix: 'lw_',
  mode: 'jit',
  theme: {
    extend: {
      colors: {
        primary: '#E95711',
        secondary: '#1A2D6D',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
